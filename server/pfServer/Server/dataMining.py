import requests
import time
import json
import requests

#API para obtener información de los nodos, utilizando su mac
#faltaria, generar un diccionario, clave mac, valor texto para tenerlo relacionado

def macApi (mac):
    urlMac = 'https://api.macvendors.com/'+ mac
    req = requests.get(url=urlMac)
    return req.text

#print(macApi(['00:11:6b:68:1c:80','c8:60:00:29:f2:ab']))

#get operative system by node (aproximación inicial "windows,GNU/linux, Otros", nos devuelve el diccionario de valores
#para obtener el so

#def getValuesSo(rasp,capture):
def getValuesSo(data,capture):
    dicSo = {}
    #data = getDataCapture(capture,rasp)
    try:
        for capt in data[capture]:
            for mac in capt.keys():
                for val in capt.values():
                    dicSo[mac]={'TTL':val['TTL'],'DF':val['DF']}
    except:
        pass
    return dicSo


#get dic with operative systems by node

def getSoByNode(dicSo):
    so = {}
    for mac in dicSo:
        if dicSo[mac]['DF'] == 'False' and dicSo[mac]['TTL'] == 128:
            so[mac] = {'SO': 'WINDOWS', 'VERSION': '', 'MOVIL': 'F', 'EMBEBIDO': 'F'}
        elif dicSo[mac]['DF'] == 'True' and dicSo[mac]['TTL'] == 64:
            so[mac] = {'SO': 'GNU/LINUX', 'VERSION': '', 'MOVIL': 'F', 'EMBEBIDO': 'F'}
        elif dicSo[mac]['DF'] == 'False':
            so[mac] = {'SO': 'OTHER - (EMBEDDED)', 'VERSION': '', 'MOVIL': 'F', 'EMBEBIDO': 'T'}
        elif dicSo[mac]['DF'] == 'True' and (dicSo[mac]['TTL'] == 1 or dicSo[mac]['TTL'] == 255):
            so[mac] = {'SO': 'OTHER - (MOBILE)', 'VERSION': '', 'MOVIL': 'T', 'EMBEBIDO': 'F'}
        else:
            so[mac] = {'SO': 'OTHER', 'VERSION': '', 'MOVIL': 'F', 'EMBEBIDO': 'F'}
    return so

#get ports by capture, return dic with key: mac. data-> datos captura actual, capture-> nombre de la captura
def getPortsByCapture(mac, data,capture):
    pSourceList = []
    pDestList = []
    for inst in data[capture]:
        try:
            pSourceList.append(inst[mac]['sport'])
            pDestList.append(inst[mac]['dport'])
        except:
            continue
    return pSourceList, pDestList

#create raw data by capture
def createDicRawData(data,capture):
    rawDic = {}
    try:
        for inst in data[capture]:
            for mac in inst.keys():
                try:
                    if inst[mac]['Raw'] != None:
                        rawDic[mac] = inst[mac]['Raw']
                except:
                    continue
    except:
        pass
    return rawDic




