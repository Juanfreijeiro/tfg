import json
import csv
import pandas as pd
#import matplotlib.pyplot as plt
#import numpy as np
#import seaborn as sns
#from deap import base
#from deap import creator
#from deap import tools
from sklearn import metrics
from sklearn.model_selection import KFold, train_test_split
from sklearn.tree import DecisionTreeClassifier, export_graphviz
#from subprocess import check_call
import pydotplus
#from IPython.display import Image
#from sklearn.externals.six import StringIO
from sklearn.ensemble import RandomForestClassifier
#from subprocess import call


#carga de fichero json
# def loadJson():
#     file = open("./datasetPrueba/2019-02-18-20:59.json","r")
#     #print(file.readlines())
#     jFile = json.loads(file.read())
#
#     return jFile

#debian -> [('MSS', 1460), ('SAckOK', b''), ('Timestamp', (3336615, 0)), ('NOP', None), ('WScale', 6)]
def test():
    print("ok")

#tranforma json en csv para utilizar como dataset
def transformOnCsv(json):
    file = open('test2.csv','w', encoding='utf-8-sig')
    mss=-1
    sakok=-1
    ts=-1
    nop=-1
    wsOpt=-1
    sport=-1
    dport=-1
    ws=-1
    f = csv.writer(file)
    f.writerow(['ipOrigen', 'DF', 'MSS', 'SAKOK', 'Ts', 'NOP','Ws.Options', 'TTL', 'Sport','Dport','ipDestino','Wsize','SO'])
    size = len(json)
    for i in range(size):
        for a in json[i].keys():
            try:
                mss= json[i][a]['Options'].find("MSS")
                sakok = json[i][a]['Options'].find("SAckOK")
                ts = json[i][a]['Options'].find("Timestamp")
                nop = json[i][a]['Options'].find("NOP")
                wsOpt = json[i][a]['Options'].find("WScale")
                ws = json[i][a]['Wsize']
                sport = json[i][a]['sport']
                dport = json[i][a]['dport']
            except:
                pass
            f.writerow([json[i][a]['IpOrigen'],
                       json[i][a]['DF'],
                       mss,
                       sakok,
                        ts,
                       nop,
                       wsOpt,
                       json[i][a]['TTL'],
                       sport,
                       dport,
                       json[i][a]['IpDestino'],
                       ws,
                        setSoCsv(json[i][a]['IpOrigen'])])
#transformOnCsv(loadJson())

def setSoCsv(ip):
    if ip == '192.168.0.22':
        return 'Windows Server'
    elif ip == '192.168.0.16':
        return 'Windows 10'
    elif ip == '192.168.0.21':
        return 'Linux/Centos'
    elif ip == '192.168.0.13':
        return 'Android 8'
    elif ip == '192.168.0.15':
        return 'Android 8'
    elif ip == '192.168.0.14':
        return 'Android 8'
    elif ip ==  '192.168.0.17':
        return 'Applel IOS'
    elif ip == '192.168.0.24':
        return 'Amazon'
    elif ip == '192.168.0.12':
        return 'Linux/Arch linux'
    elif ip == '192.168.0.11':
        return 'Linux/Android Wear'
    elif ip == '192.168.0.23':
        return 'Linux/Tizen wareable'
    elif ip == '192.168.0.1':
        return 'SO Embebed'
    elif ip == '192.168.0.4':
        return 'Linux/Raspbian'


def loadDataCsv():
    data = pd.read_csv("./Server/test2.csv", sep=",", header=0, encoding='utf-8', dtype='object')
    return data

#categorizacion TTL y DF
def dataCategorized(filterData):
    onliFilterData=pd.DataFrame(filterData.drop(columns=['DF','MSS','SAKOK','Ts','NOP',
                                                         'Ws.Options','TTL','Wsize','SO']))
    #filterData['ttlEncoded'] = filterData['TTL'].map({'64':0, '128':1,'255':2,'1':3,'':4}).astype('int64')
    #filterData['dfEncoded'] = filterData['DF'].map({'True':1,'False':0,'':2}).astype('int64')
    ttl = {'64':0, '128':1,'255':2,'1':3,'':4}
    df = {'True':1,'False':0,'':2}
    so = {'Linux/Arch linux':0,'Windows 10':1,'SO Embebed':2,'Android 8':3}
    mss = {'-1':0}
    sak = {'-1':0}
    ts = {'-1':0}
    nop = {'-1':0}
    wsO = {'-1':0}
    wsize = {'1444': 0, '292': 1, '879': 2, '350':3,'-1': 4 }
    for i in filterData['TTL']:
        if i not in ttl:
            ttl[i] = len(ttl)
    for o in filterData['DF']:
        if o not in df:
            df[o] = len(df)
    for u in filterData['SO']:
        if u not in so:
            so[u]=len(so)
    for mssRow in filterData['MSS']:
        if mssRow not in mss:
            mss[mssRow] = len(mss)
    for sakRow in filterData['SAKOK']:
        if sakRow not in sak:
            sak[sakRow] = len(sak)
    for tsRow in filterData['Ts']:
        if tsRow not in ts:
            ts[tsRow]=len(ts)
    for nopRow in filterData['NOP']:
        if nopRow not in nop:
            nop[tsRow] = len(nop)
    for wsoRow in filterData['Ws.Options']:
        if wsoRow not in wsO:
            wsO[wsoRow] = len(wsO)
    for wsizeRow in filterData['Wsize']:
        if wsizeRow not in wsize:
            wsize[wsizeRow]=len(wsize)
    onliFilterData['dfEncoded'] = filterData['DF'].map(df).astype('int64')
    onliFilterData['mssEncoded'] = filterData['MSS'].map(mss).astype('int64')
    onliFilterData['sakEncoded'] = filterData['SAKOK'].map(sak).astype('int64')
    onliFilterData['tsEncoded'] = filterData['Ts'].map(ts).astype('int64')
    onliFilterData['nopEncoded'] = filterData['NOP'].map(nop)
    onliFilterData['wsoEncoded'] = filterData['Ws.Options'].map(wsO).astype('int64')
    onliFilterData['ttlEncoded'] = filterData['TTL'].map(ttl).astype('int64')
    onliFilterData['wsizeEncoded'] = filterData['Wsize'].map(wsize).astype('int64')
    onliFilterData['soEncoded'] = filterData['SO'].map(so)

    return onliFilterData,ttl,df,so,mss,sak,ts,nop,wsO,wsize

def testDecisionTree(data):
    #print(data.head())
    delete = ['ipOrigen', 'MSS', 'SAKOK', 'Ts', 'NOP', 'Ws.Options', 'Sport', 'Dport', 'ipDestino', 'Wsize']
    analisysFrame = data.drop(columns=delete)
    #print(analisysFrame)
    #print(analisysFrame["TTL"].value_counts())
    dataCat= dataCategorized(analisysFrame)
    #print(dataCat.head())
    #print(dataCat['ttlEncoded'].groupby(['ttlEncoded']), as_index=False).agg(['mean', 'count', 'sum'])
    #print(dataCat.groupby('ttlEncoded', as_index=False).agg(['mean', 'count', 'sum']))
    #construccion de arbol de decision
    accuracies = list()
    cv = KFold(n_splits=10)
    max_attributes = len(list(dataCat))
    depth_range = range(1, max_attributes + 1)

    for depth in depth_range:
        fold_accuracy = []
        tree_model = DecisionTreeClassifier(criterion='entropy',
                                                 min_samples_split=20,
                                                 min_samples_leaf=5,
                                                 max_depth=depth,
                                                 class_weight={1: 3.5})
        for train_fold, valid_fold in cv.split(dataCat):
            f_train = dataCat.loc[train_fold]
            #print(f_train)
            f_valid = dataCat.loc[valid_fold]
            #print(f_valid)

            model = tree_model.fit(X=f_train.drop(['ttlEncoded'], axis=1),
                                   y=f_train["ttlEncoded"])
            #print(model)
            valid_acc = model.score(X=f_valid.drop(['ttlEncoded'], axis=1),
                                    y=f_valid["ttlEncoded"])  # calculamos la precision con el segmento de validacion
            fold_accuracy.append(valid_acc)

        avg = sum(fold_accuracy) / len(fold_accuracy)
        accuracies.append(avg)

    # Mostramos los resultados obtenidos
    df = pd.DataFrame({"Max Depth": depth_range, "Average Accuracy": accuracies})
    df = df[["Max Depth", "Average Accuracy"]]
    print(df.to_string(index=False))


def printDecisionTree():
    csv = loadDataCsv()
    delete = ['ipOrigen', 'MSS', 'SAKOK', 'Ts', 'NOP', 'Ws.Options', 'Sport', 'Dport', 'ipDestino', 'Wsize']
    analisysFrame = csv.drop(columns=delete)
    dataCat = dataCategorized(analisysFrame)
    y_train = dataCat['ttlEncoded']
    x_train = dataCat.drop(['ttlEncoded'], axis=1).values
    # Crear Arbol de decision con profundidad = 4
    decision_tree = DecisionTreeClassifier(criterion='entropy',
                                                min_samples_split=20,
                                                min_samples_leaf=5,
                                                max_depth=4,
                                                class_weight={1: 3.5})
    decision_tree.fit(x_train, y_train)
    # exportar el modelo a archivo .dot
    with open(r"tree1.dot", 'w') as f:
        f = export_graphviz(decision_tree,
                                 out_file=f,
                                 max_depth=4,
                                 impurity=True,
                                 feature_names=list(dataCat.drop(['ttlEncoded'], axis=1)),
                                 class_names=None,
                                 rounded=True,
                                 filled=True)
    graph = pydotplus.graph_from_dot_file('tree1.dot')
    graph.write_png('tree1.png')
    # Image(graph.create_png())
    # # Convertir el archivo .dot a png para poder visualizarlo
    # check_call(['dot', '-Tpng', r'tree1.dot', '-o', r'tree1.png'])
    #PImage("tree1.png")

def randomForest():
    csv = loadDataCsv()
    delete = ['ipOrigen', 'MSS', 'SAKOK', 'Ts', 'NOP', 'Ws.Options', 'Sport', 'Dport', 'ipDestino', 'Wsize']
    analisysFrame = csv.drop(columns=delete)
    dataCat = dataCategorized(analisysFrame)
    x = dataCat[['dfEncoded','ttlEncoded']]
    y = dataCat.drop(['ttlEncoded'], axis=1)
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.3)

    clf = RandomForestClassifier(n_estimators=100)
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    print(y_pred)
    print("Accuracy:", metrics.accuracy_score(y_test, y_pred))

def testDecisionTree2(data, listTest):
    #DF,TTL,MSS,SAKOK,TS,NOP,WS.OPTIONS,WSIZE
    soList = list()
    #xPrueba = [1,0,0,0,1,1,0,1]
    delete = ['ipOrigen', 'Sport', 'Dport', 'ipDestino']
    analisysFrame = data.drop(columns=delete)
    dataCat,ttl,df,so,mss,sak,ts,nop,wsO,wsize = dataCategorized(analisysFrame)
    #print(dataCat)
    x = dataCat[['dfEncoded', 'mssEncoded', 'sakEncoded', 'tsEncoded', 'nopEncoded',
                 'wsoEncoded', 'ttlEncoded','wsizeEncoded']].fillna(400)
    y = dataCat['soEncoded'].fillna(400)
    xVal = x.values
    yVal = y.values

    X_train, X_test, y_train, y_test = train_test_split(xVal, yVal, test_size=0.3)
    #print(X_test)

    #arbol = DecisionTreeClassifier()
    arbol = RandomForestClassifier(n_estimators=100)
    arbol.fit(X_train,y_train)
    y_pred = arbol.predict(X_test)
    #print(ttl)
    # print(so)
    # print(mss)
    # print(sak)
    # print(ts)
    # print(nop)
    # print(wsO)
    #print(wsize)
    print("Accuracy:", metrics.accuracy_score(y_test, y_pred))
    accuracy = metrics.accuracy_score(y_test, y_pred)
    #print(listTest)
    for test in listTest:
        predSo=arbol.predict([test])
        #print(arbol.predict([test]))
        #print(test)
        for k,i in so.items():
            if predSo == i:
                #print(k)
                soList.append(k)
    return soList, accuracy
    #obtenmos la importancia que tiene cada caracteristica para el algoritmo utilizado
    # feature_imp = pd.Series(arbol.feature_importances_,index=['dfEncoded','ttlEncoded', 'mssEncoded', 'sakEncoded', 'tsEncoded', 'nopEncoded',
    #              'wsoEncoded','wsizeEncoded']).sort_values(ascending=False)
    # print(feature_imp)
    # sns.barplot(x=feature_imp, y=feature_imp.index)
    # plt.xlabel('Importancia de cada caracteristica')
    # plt.ylabel('Caracteristicas')
    # plt.title("Muestra de la importancia dada a cada caracteristica")
    # plt.legend()
    # plt.show()

    #imprimir arbol
    # print(so)
    # estimator = arbol.estimators_[5]
    # export_graphviz(estimator, out_file='arbol.dot',feature_names =['dfEncoded','ttlEncoded', 'mssEncoded', 'sakEncoded', 'tsEncoded', 'nopEncoded',
    #               'wsoEncoded','wsizeEncoded'],class_names=list(so.keys()))
    # call(['dot', '-Tpng', 'arbol.dot', '-o', 'arbol.png', '-Gdpi=600'])
    # Image(filename='arbol.png')


#A partir de una mac obtener todas las capturas de un nodo
def getAllCaptures(mac,data):
    cap = data
    #print(cap)
    capList = list()
    for inst in range(len(cap)):
        try:
            if mac in cap[inst].keys():
                #print(cap[inst])
                capList.append(cap[inst])
        except:
            continue
    return capList

#categorizar todas las capturas de un nodo
def categorizedAllCaptures(capList):
    # DF,TTL,MSS,SAKOK,TS,NOP,WS.OPTIONS,WSIZE
    categorizedCapAll = list()
    mssFind = '-1'
    sakFind = '-1'
    tsFind = '-1'
    nopFind = '-1'
    wsOFind = '-1'
    wsFind = '-1'

    ttl = {'64': 0, '128': 1, '255': 2, '1': 3, '': 4}
    df = {'True': 1, 'False': 0, '': 2}
    mss = {'-1': 0}
    sak = {'-1': 0}
    ts = {'-1': 0}
    nop = {'-1': 0}
    wsO = {'-1': 0}
    wsize = {'1444': 0, '292': 1, '879': 2, '350': 3, '-1': 4}
    #print(capList)
    for inst in range(len(capList)):
        for cap in capList[inst].keys():
            categorizedCap = list()

            o = capList[inst][cap]['DF']
            if o not in df:
                df[o] = len(df)
            categorizedCap.append(df[o])

            try:
                mssFind = str(capList[inst][cap]['Options'].find("MSS"))
                sakFind = str(capList[inst][cap]['Options'].find("SAckOK"))
                tsFind = str(capList[inst][cap]['Options'].find("Timestamp"))
                nopFind = str(capList[inst][cap]['Options'].find("NOP"))
                wsOFind = str(capList[inst][cap]['Options'].find("WScale"))
                wsFind = str(capList[inst][cap]['Wsize'])
            except:
                pass


            if mssFind not in mss:
                mss[mssFind] = len(mss)
            categorizedCap.append(mss[mssFind])

            if sakFind not in sak:
                sak[sakFind] = len(sak)
            categorizedCap.append(sak[sakFind])

            if tsFind not in ts:
                ts[tsFind] = len(ts)
            categorizedCap.append(ts[tsFind])

            if nopFind not in nop:
                nop[nopFind] = len(nop)
            categorizedCap.append(nop[nopFind])

            if wsOFind not in wsO:
                wsO[wsOFind] = len(wsO)
            categorizedCap.append(wsO[wsOFind])

            i = str(capList[inst][cap]['TTL'])
            if i not in ttl:
                ttl[i] = len(ttl)
            categorizedCap.append(ttl[i])
            #inicializar variables como None para establecer valores de no existencia
            if wsFind not in wsize:
                #print(wsFind)
                wsize[wsFind] = len(wsize)
            categorizedCap.append(wsize[wsFind])

            categorizedCapAll.append(categorizedCap)

    return categorizedCapAll

#obtenemos el SO con el numero de ocurrencias maximo
def maxSo(soList):
    maxList = list()
    for soPos in range(len(soList)):
        maxList.append((soList.count(soList[soPos]),soPos))
    soMax = max(maxList)
    maxSo,pos = soMax
    #print(soList)
    return soList[pos]

def percentSo(soList):
    soUnique = list()
    percent = list()
    soDic = {}
    #calcular el 100%
    total = len(soList)
    for so in soList:
        if so not in soUnique:
            soUnique.append(so)
    for i in range(len(soUnique)):
        percent.append((soList.count(soUnique[i])*100)/total)
    #print(len(soUnique))
    #print(percent)
    for soD in range(len(soUnique)):
        soDic[soUnique[soD]]=percent[soD]
    return soDic

#generemaos CSV de aprendizaje a partir del json
#json=loadJson()
#transformOnCsv(json)
#testDecisionTree2(loadDataCsv())

#Ejecutar imprimir todas las capturas de un nodo
#capList= getAllCaptures('70:5a:9e:90:68:69')
#catList = categorizedAllCaptures(capList)
#for i in range(5):
#so = testDecisionTree2(loadDataCsv(),catList)
#print(maxSo(so))