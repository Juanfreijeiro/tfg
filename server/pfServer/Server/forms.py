from django import forms
from .models import Usuarios

class ValidateForm(forms.Form):
    login = forms.CharField(label='login', max_length=100)
    password = forms.CharField(label='password', widget=forms.PasswordInput, max_length=100)
    class Meta:
        model = Usuarios

class InfoNode(forms.Form):
    so = forms.CharField(label='login', max_length=100)
    version = forms.CharField(label='login', max_length=100)
    macVendor = forms.CharField(label='login', max_length=100)
    ports = forms.CharField(label='login', max_length=100)
    posibleServices = forms.CharField(label='login', max_length=100)
    otherInfo = forms.CharField(label='login', max_length=100)
