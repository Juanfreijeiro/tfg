from django.shortcuts import render
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect, HttpResponseNotFound
from .forms import ValidateForm
from passlib.hash import argon2
from Server.models import Usuarios
from .dataHandler import *
from .dataMining import *
from .config import server
from django.urls import reverse
from .agExplotation import *



def index(request):
    captureList = []
    dateCaptures = None
    datePrint=''
    try:
        form = ValidateForm(request.POST)
        try:
            ip = request.session['ip']
            sesion = request.session['sesion']
        except:
            request.session['ip'] = server['raspberrys']['rasp1']
            ip = request.session['ip']
            sesion='none'
            pass
        try:
            capture = request.session['capture']
            captureList.append(capture)
        except:
            capture = None
            pass
        #print(capture)
        #print(captureList)

        if sesion != 'verify':
            #print("distinto?")
            noValid = "No valid User or Password. Try again..."
            return render(request, 'login.html', {'form': form,'valid':noValid})
        else:
            name = getRaspyName(ip)
            #print(name)getMetadataDatesIni()

            if name != 'clean' and capture is None:
                lastDate = getLastCaptureInMetadata(name)
                nodeList, inputEdges, conn = getPlotlyElements(name,lastDate)
                dateCaptures = getMetadataDatesIni(getMetadata(name))
                datePrint = lastDate
                print("paso1")

            elif capture != None and name != 'clean':
                nodeList, inputEdges, conn = getPlotlyElements(name,capture)
                #selectMeta = getSelectMetadata(capture, getMetadata(name))
                dateCaptures = getMetadataDatesIni(getMetadata(name))
                datePrint = capture
            else:
                inputEdges = 0
                nodeList = []
                conn = []
                dateCaptures = ''
                dateCaptures = None
                request.session['capture'] = None
            #print(name)
            map = MapPlotly(nodeList,inputEdges, conn,name,datePrint)
            # counters
            if nodeList:
                totalNodes = len(nodeList)
                # meta = getMetadata(rasp)
                # sMeta = getSelectMetadata(capture,meta)
                # tSmeta = sMeta['ficheros_captura']
                #so = callExplotation(datePrint,name)
                #print(rasp)
                #print(capture)
                if capture != None:
                    tSo = totalCaptureFiles(name, capture)
                    #print(tSo)
                    so = dicUniqueForFiles(tSo)
                else:
                    so = {}
                l = 0
                w = 0
                o = 0
                #print(nodeList)
                try:
                    for mac in nodeList:
                        if so[mac]['SO'] == 'WINDOWS':
                            w +=1
                        elif so[mac]['SO'] == 'GNU/LINUX':
                            l +=1
                        else:
                            o +=1
                except:
                    o+=1
                    pass

            else:
                totalNodes = 0
                l = 0
                w = 0
                o = 0
                #print(capture)
            # if capture is not None and name != 'clean':
            #     selectMeta = getSelectMetadata(capture ,getMetadata(name))
            #     dateCaptures = captureList.append(selectMeta['fecha_ini'])
            # else:
            #     request.session['capture'] = None

            #print(dateCaptures)

            user, image = Usuarios.objects.values()[0]['login'], Usuarios.objects.values()[0]['image']
            return render(request, 'index.html', {'map': map, 'user':user, 'image':image,'totalNodes':totalNodes,
                                                   'raspberrys':server['raspberrys'].keys(),'captures':dateCaptures,
                                                  'linuxCount': l, 'windowsCount': w, 'otherCount': o})
    except Exception as e:
        #return render(request, 'login.html', {'form': form})
        print("entra excepcion : " + str(e))


def login(request):
        # if this is a POST request we need to process the form data
        if request.method == 'POST':
            # create a form instance and populate it with data from the request:
            form = ValidateForm(request.POST)
            # check whether it's valid:
            if form.is_valid():
                passForm=form.cleaned_data['password']
                passEncrypt = Usuarios.objects.values()[0]['password']
                userForm = form.cleaned_data['login']
                userDDBB = Usuarios.objects.values()[0]['login']
                #comprobamos si la contraseña es valida
                if argon2.verify(passForm,passEncrypt) and (userForm == userDDBB):
                    try:
                        ip = request.session['ip']
                        #print("ip try "+ str(ip))
                    except:
                        #request.session['ip'] = server['raspberrys']['rasp1']
                        #ip = request.session['ip']
                        ip = server['raspberrys']['rasp1']
                    #print("que coño devuelve getRaspyName? :" + getRaspyName(ip))
                    name = getRaspyName(ip)
                    #print(name + "ip???")
                    # if name != 'clean':
                    lastDate = getLastCaptureInMetadata(name)
                    #     nodeList, inputEdges, conn = getPlotlyElements(name,lastDate)
                    #     #dateCaptures = getMetadataDatesIni(getMetadata(name))
                    #     #print(dateCaptures)
                    # else:
                    inputEdges = 0
                    nodeList = []
                    conn = []
                    #dateCaptures = ''
                    #print(inputEdges)

                    map = MapPlotly(nodeList, inputEdges, conn,name,lastDate)
                    user,image=Usuarios.objects.values()[0]['login'],Usuarios.objects.values()[0]['image']
                    request.session['sesion'] = 'verify'
                    #counters
                    if nodeList:
                        totalNodes = len(nodeList)
                    else:
                        totalNodes = ''
                    #print(ip)

                    #print(dateCaptures)
                    return render(request, 'index.html', {'map': map, 'user':user, 'image':image,
                                                          'totalNodes':totalNodes,
                                                          'raspberrys':server['raspberrys'].keys()})
                                                          #'captures':dateCaptures})
                else:
                    noValid = "No valid User or Password. Try again..."
                    return render(request, 'login.html', {'form': form, "valid": noValid})

        # if a GET (or any other method) we'll create a blank form
        else:
            form = ValidateForm()


        return render(request, 'login.html', {'form': form})

def logout(request):
    form = ValidateForm(request.POST)
    try:
        ip = request.session['ip']
    except:
        ip = None
    request.session.flush()
    return render(request, 'login.html', {'form': form, 'ip':ip})

def rasp(request,ip):
    try:
        #print('rasp ' + ip)
        form = ValidateForm(request.POST)
        sesion = request.session['sesion']
        request.session['ip']=ip
        #print(request.session['sesion'])

        if sesion != 'verify':
            return render(request, 'login.html', {'form': form})
        else:
            return  HttpResponseRedirect(reverse('index'))
            # nodeList, inputEdges, conn = getPlotlyElements(ip)
            #
            # map = MapPlotly(nodeList, inputEdges, conn)
            # user, image = Usuarios.objects.values()[0]['login'], Usuarios.objects.values()[0]['image']
            # print(request.POST)
            # totalNodes = len(nodeList)
            # return  render(request, 'index.html', {'map': map, 'user':user, 'image':image,
            #                                               'totalNodes':totalNodes,
            #                                    'raspberrys':server['raspberrys'],'raspIp':ip})
    except:
        return HttpResponseNotFound('<h1>Page not found</h1>')

def refresh(request):
    try:
        form = ValidateForm(request.POST)
        try:
            ip = request.session['ip']
            sesion = request.session['sesion']
        except:
            request.session['ip'] = server['raspberrys']['rasp1']
            ip = request.session['ip']
            sesion='none'
            pass
        #print(ip)
        if sesion != 'verify':
            #print("distinto?")
            noValid = "No valid User or Password. Try again..."
            return render(request, 'login.html', {'form': form,'valid':noValid})
        else:
            name = getRaspyName(ip)
            #print(name)
            if name != 'clean':
                nodeList, inputEdges, conn = getPlotlyElements(name)
                dateCaptures = getMetadataDatesIni(getMetadata(name))
            else:
                inputEdges = 0
                nodeList = []
                conn = []
                dateCaptures = ''

            map = MapPlotly(nodeList,inputEdges, conn,name)
            # counters
            if nodeList:
                totalNodes = len(nodeList)
            else:
                totalNodes = 0
            return render(request, 'index.html', {'map': map,'totalNodes':totalNodes,'captures':dateCaptures})
    except Exception as e:
        print(e)

def capture(request,date):
    try:
        #print('rasp ' + ip)
        form = ValidateForm(request.POST)
        sesion = request.session['sesion']
        request.session['capture']= date

        if sesion != 'verify':
            return render(request, 'login.html', {'form': form})
        else:
            return  HttpResponseRedirect(reverse('index'))
            # nodeList, inputEdges, conn = getPlotlyElements(ip)
            #
            # map = MapPlotly(nodeList, inputEdges, conn)
            # user, image = Usuarios.objects.values()[0]['login'], Usuarios.objects.values()[0]['image']
            # print(request.POST)
            # totalNodes = len(nodeList)
            # return  render(request, 'index.html', {'map': map, 'user':user, 'image':image,
            #                                               'totalNodes':totalNodes,
            #                                    'raspberrys':server['raspberrys'],'raspIp':ip})
    except:
        print("fallo")

def info(request,mac):
    pDic = []
    rDic = []
    try:
        form = ValidateForm(request.POST)
        sesion = request.session['sesion']
        #print(mac)

        if sesion != 'verify':
            return render(request, 'login.html', {'form': form})
        else:
            macClic, ip, cap, rasp = mac.split('&')
            user, image = Usuarios.objects.values()[0]['login'], Usuarios.objects.values()[0]['image']
            vendor = macApi(macClic)
            #so = callExplotation(cap, rasp)
            meta = getMetadata(rasp)
            sMeta = getSelectMetadata(cap, meta)
            tSmeta = sMeta['ficheros_captura']
            tSo = totalCaptureFiles(rasp, cap)
            so = dicUniqueForFiles(tSo)
            #print(sMeta)
            #print(tSmeta)
            for cFile in tSmeta:
                data = getDataCapture(cFile,rasp)
                try:
                    capList=getAllCaptures(macClic, data[cFile])
                    pDic.append(processPorts(data, cFile))
                    rDic.append(createDicRawData(data, cFile))
                except:
                    continue
            catList=categorizedAllCaptures(capList)
            listSoGA, accuracy = testDecisionTree2(loadDataCsv(), catList)
            soT = percentSo(listSoGA)
            #print(rDic)
            #print(pDic)
            portDic =  dicUniqueForFiles(pDic)
            rawDic = dicUniqueForFiles(rDic)
            #print(portDic)
            #print(rawDic)
            try:
                lNameSource = []
                lDescSource = []
                lNameTarget = []
                lDescTarget = []
                serviceSource = [lookupIanaPorts(str(port)) for port in portDic[macClic]['pSource']]
                serviceTarget = [lookupIanaPorts(str(port)) for port in portDic[macClic]['pDest']]
                for s in serviceSource:
                    name,description = s
                    lNameSource.append(name)
                    lDescSource.append(description)
                for t in serviceTarget:
                    nameT,descriptionT = t
                    lNameTarget.append(nameT)
                    lDescTarget.append(descriptionT)
            except Exception as e:
                print("peta" + str(e))
                pass

            try:
                raw = rawDic[macClic]
            except:
                raw = None

            return render(request, 'nodeInfo.html', {'ipInfo': ip,'user': user, 'image': image,'vendor':vendor,
                                                      'SO':so[macClic]['SO'],'mac':macClic,
                                                     'sPorts':portDic[macClic]['pSource'],
                                                     'dPorts':portDic[macClic]['pDest'],
                                                     'serviceSource':lNameSource,'descSource':lDescSource,
                                                     'serviceTarget': lNameTarget, 'descTarget': lDescTarget,
                                                     'Raw': raw, 'accuracy': accuracy, 'soT':soT})
    except:
        return HttpResponseNotFound('<h1>Page not found</h1>')