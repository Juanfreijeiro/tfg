import json
import requests
import plotly.plotly as py
import networkx as nx
from plotly.offline import download_plotlyjs, init_notebook_mode,  iplot, plot
import operator
import csv
from .dataMining import *
from .config import server
init_notebook_mode(connected=True)

#check if exists raspberry ip
def checkRaspberryIpPosition(rasp):
    cont = 0
    try:
        for ip in server['raspberrys']:
            if ip == rasp:
                return cont
            cont += 1
        return 0
    except:
        print("invalid Ip")

#get json files from Raspberry (API)
def getJsonFiles(rasp):
    try:
        req = requests.get(url='http://'+server['raspberrys'][rasp]+server['endpointFiles'])
        return req.json()
    except:
        print('Punto de acceso -files- no activo')


#get dictionary from files (API)
def getDataCapture(fileName,rasp):
    fileName = fileName.replace('.json','')
    try:
        urlFile = 'http://'+server['raspberrys'][rasp]+server['endpointCapture'] + fileName
        req = requests.get(url=urlFile)
    except:
        print('Punto de acceso -capture- no activo')
    return req.json()

#get json metada all captures
def getMetadata(rasp):
    try:
        rasp = getRaspyName(rasp)
        urlFile = 'http://'+server['raspberrys'][rasp]+server['endpointMetadata']
        req = requests.get(url=urlFile)
        return req.json()
    except:
        print('Punto de acceso -metadata- no activo')


#get dates ini
def getMetadataDatesIni(meta):
    dateList = []
    for date in meta:
        dateList.append(meta[date]['fecha_ini'])
    return dateList

#get select dates from metadata
def getSelectMetadata(date, meta):
    for dic in meta:
        if meta[dic]['fecha_ini'] == date:
            return meta[dic]


#get node number (macs) from file json
def getNumNodes(data,filename):
    filename = filename.replace('.json', '')
    nodeList = []
    try:
        for tuple in data[filename]:
            for mac in tuple.keys():
                nodeList.append(mac)
    except:
        pass
    return nodeList

#get connections
def connectionNodes(nodes, data, filename):
    filename = filename.replace('.json', '')
    count=0
    connection = []
    try:
        for node in nodes:
            connection.append((data[filename][count][node]['IpOrigen'],data[filename][count][node]['IpDestino'],
                               [mac for mac in data[filename][count].keys()][0]))
            count += 1
    except:
        pass
    return connection

#get edges
def getEdges(connNodes, categorized):
    distNodesA = set(connNodes)
    distNodesB = set(connNodes)
    edges =[]
    for l,m,n in distNodesA:
        for x,y,z in distNodesB:
            if m == x:
                edges.append((categorized[n],categorized[z]))
        #broadcast packets
        # if m == '255.255.255.255':
        #     for i,j,k in distNodesA:
        #         if k != n:
        #             edges.append((categorized[n],categorized[k]))
    return edges

def categorizedNodes(nodeList):
    dic={}
    count=0
    distinctNodes = set(nodeList)
    for mac in distinctNodes:
        dic[mac]=count
        count +=1
    return dic

#get name from raspberry with your ip
def getRaspyName(ip):
    #print(type(ip))
    result=""
    for key in server['raspberrys']:
        if ip in server['raspberrys'][key]:
            result = key
        else:
            result= ip
    return result

#sorted category dic
def sortedNodesByCat(sortedCat):
    sList=[]
    for cat in range(len(sortedCat)):
        mac,order=sortedCat[cat]
        sList.append((mac,"\n Category: "+ str(order)))
    return sList

#sorted by connections
def sortedNodesByConnections(conn,labels):
    sList =[]
    for mac in labels:
        for ipO,ipD,macC in conn:
            if mac == macC:
                sList.append(ipO,ipD,macC)
    return sList

#max edges position, coordenades x and y
def maxEdgesPosition(iEdges):
    aList = []
    bList = []
    for eC in iEdges:
        x,y = eC
        aList.append(x)
        bList.append(y)
    a = max(aList)
    b = max(bList)
    return a,b

#remove repeeted tuples
def removeDuplicatedEdges(edges):
    tEdges = []
    for oEdges in edges:
        for iEdges in edges:
            if oEdges[::-1] == iEdges and iEdges in tEdges:
                break
            tEdges.append(oEdges)
    return list(set(tEdges))

#get all ip dest from nodes
def getAllIpDest(nodes, conn):
    ipDest = []
    ipDestDict = {}
    for node in nodes.keys():
        for ipO,ipD,mac in conn:
            if node == mac:
                ipDest.append(ipD)
        ipDestDict[node]=list(set(ipDest))
        ipDest=[]
    return ipDestDict

#get jsonFiles metadata from specific capture
def getJsonFilesByCapture(rasp,capture):
    metaDic = getMetadata(rasp)
    for cap in metaDic:
        if metaDic[str(cap)]['fecha_ini'] == capture:
            return metaDic[str(cap)]

#get last date ini from last capture
def getLastCaptureInMetadata(rasp):
    metaDic = getMetadata(rasp)
    valueMeta = []
    try:
        for val in metaDic:
            valueMeta.append(int(val))
        maxDate = metaDic[str(max(valueMeta))]['fecha_ini']
    except:
        maxDate = 'XX/XX/XXXX'
    return  maxDate

#call dataMining SOs
def callExplotation(capture,rasp):
    data = getDataCapture(capture, rasp)
    dicSo = getValuesSo(data,capture)
    so = getSoByNode(dicSo)
    return so

#get and process ports from dataMining
def processPorts(data, capture):
    pSourceList = []
    pDestList = []
    portDic = {}
    try:
        for inst in data[capture]:
            for mac in inst.keys():
                pSourceList, pDestList = getPortsByCapture(mac,data,capture)
                portDic[mac] = {'pSource':list(set(pSourceList)),'pDest':list(set(pDestList))}
    except:
        pass
    return portDic

#Search ports in data base IANA (file .csv) -> se Busca en la base de datos de IANA si el puerto está registrádo y se
#devuelve a que servicio se encuentra registrado y en que protocolo de transporte está asociado
def lookupIanaPorts(port):
    with open('/home/vagrant/tfgpf/tfg/server/pfServer/Server/service-names-port-numbers.csv', 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            try:
                if port == row[1]:
                    service = row[0]
                    description = row[3]
                    return service, description
            except:
                continue
        return 'Service not found','Port not found in the IANA database'

#get all nodes from total capture files
def totalCaptureFiles(rasp,capture):
    tSo = []
    meta = getMetadata(rasp)
    sMeta = getSelectMetadata(capture, meta)
    tSmeta = sMeta['ficheros_captura']
    try:
        for cap in tSmeta:
            so = callExplotation(cap, rasp)
            tSo.append(so)
    except:
        pass
    return tSo

#dic unique for all files
def dicUniqueForFiles(dicSo):
    listMac = []
    tSoDic = {}
    for cont in range(len(dicSo)):
        for mac in dicSo[cont].keys():
            if mac not in listMac:
                tSoDic[mac]=dicSo[cont][mac]
                listMac.append(mac)
    return tSoDic




#print(lookupIanaPorts('22'))
#--------------------------------------------------
#return distinct elements for list -> set(list)
#--------------------------------------------------

def getPlotlyElements(rasp,capture):
    nodeList=[]
    conn = []
    nodeListTotal=[]
    #cont = checkRaspberryIpPosition(rasp)
    #print("getPloty :" + str(cont))
    try:
        #print("antes " + rasp)
        rasp = getRaspyName(rasp)
        #print(rasp)
        filename = getJsonFilesByCapture(rasp,capture)['ficheros_captura']
        for jsonFile in filename:
            data = getDataCapture(jsonFile,rasp)
            nodeList = getNumNodes(data, jsonFile)
            nodeListTotal = list(set(nodeList + nodeListTotal))
            conn = list(set(connectionNodes(nodeList,data,jsonFile) + conn))
        #print(nodeListTotal)
        cat=categorizedNodes(nodeListTotal)
        inputEdges=getEdges(conn,cat)
        return cat, inputEdges, conn
    except Exception as e:
        print(e)
        inputEdges=0
        nodeList=[]
        conn = []
        return nodeList,inputEdges, conn
#plotly

def MapPlotly(nodeList,inputEdges, conn,rasp,capture):
    try:
        dicLink = {}
        G = nx.Graph()
        ipsRel = []
        ipsRelcat = []
        orderMacs = []
        my_nodes = range(len(nodeList))
        G.add_nodes_from(my_nodes)
        my_edges = removeDuplicatedEdges(inputEdges)
        G.add_edges_from(my_edges)
        #x,y = maxEdgesPosition(my_edges)
        #G.add_edge(x,y)
        #G.add_edge(4, 4)
        #print(nodeList)
        #print(conn)
        #print(inputEdges)
        #print(my_edges)
        #print(my_nodes)
        title = 'Network Graph<br>'+ rasp + ' : ' + capture
        pos = nx.fruchterman_reingold_layout(G)

        sortedLabels = sorted(nodeList.items(), key=operator.itemgetter(1))
        labels = sortedNodesByCat(sortedLabels)
        multiIpDest = getAllIpDest(nodeList,conn)
        #print(multiIpDest)

        #SOs by node
        #so = callExplotation(capture,rasp)
        #print(rasp)
        #print(capture)
        tSo=totalCaptureFiles(rasp,capture)
        print(tSo)
        dicSo = dicUniqueForFiles(tSo)
        #print(conn)
        #print(labels)
        #relations mac, ips
        for mac,order in labels:
            for ipO, ipD, macC in conn:
                if mac == macC:
                    if (ipO,mac) not in ipsRel:
                        ipsRel.append((ipO,mac))
                        #ipsRelcat.append(ipO + order)
                        ipsRelcat.append(ipO)
                        #orderMacs.append(macC + "</br> Ip destino :"+ipD)
                        #si la mac no existe en el loop continuamos
                        try:
                            for macD in multiIpDest:
                                if macD == macC:
                                    orderMacs.append("MAC: "+macC + "</br>Target IP/s: "+ str(multiIpDest[macD])
                                                     + "</br>OS: " + str(dicSo[macC]['SO']))
                        except Exception as e:
                            print("peta"+ str(e))



        #print(ipsRel)
        #print(orderMacs)



        #Xn = [pos[k][0] for k in range(len(pos))]
        #Yn = [pos[k][1] for k in range(len(pos))]
        Xn = [pos[k][0] for k in range(len(my_nodes))]
        Yn = [pos[k][1] for k in range(len(my_nodes))]

        trace_nodes = dict(type='scatter',
                           x=Xn,
                           y=Yn,
                           mode='markers+ text',
                           #mode='markers',
                           marker=dict(symbol='circle-dot', size=35, color='#6959CD'),
                           text=["<a href='info/"+str(m)+"&"+str(i)+"&"+str(capture)+"&"+str(rasp)+"' target=''>"
                                 +str(i)+"</a>" for i,m in ipsRel],
                           #text=ipsRelcat,
                           #hoverinfo='text',
                           hovertext=orderMacs,
                           textposition='top center',
                           name='',
                           showlegend=False
                           )


        Xe = []
        Ye = []
        #for e in G.edges():
        for e in my_edges:
            Xe.extend([pos[e[0]][0], pos[e[1]][0], None])
            Ye.extend([pos[e[0]][1], pos[e[1]][1], None])

        trace_edges = dict(type='scatter',
                           mode='lines',
                           x=Xe,
                           y=Ye,
                           line=dict(width=2, color='#000000'),
                           hoverinfo='none',
                           showlegend=False
                           )

        xxaxis = dict(showline=False,  # hide axis line, grid, ticklabels and  title
                    zeroline=False,
                    showgrid=False,
                    showticklabels=False,
                    visible=False,
                    ticks='',
                    )
        yyaxis = dict(showline=False,  # hide axis line, grid, ticklabels and  title
                    zeroline=False,
                    showgrid=False,
                    showticklabels=False,
                    visible=False,
                    ticks='',
                    )
        layout = dict(title= title,
                      font=dict(family='Balto'),
                      width=1024,
                      height=700,
                      autosize=False,
                      showlegend=True,
                      xaxis=xxaxis,
                      yaxis=yyaxis,
                      margin=dict(
                          l=40,
                          r=40,
                          b=85,
                          t=100,
                          pad=0,

                      ),
                      hovermode='closest',
                      plot_bgcolor='#E7E7E7',  # set background color
                      )

        fig = dict(data=[trace_edges, trace_nodes], layout=layout)

        #map=plot(figure_or_data=fig, filename='./templates/networkMap.html', auto_open=False, show_link=False,
        #     output_type='div')
        map = plot(figure_or_data=fig, auto_open=False, show_link=False, output_type='div')
        return map
    except Exception as e:
        print(e)
        print("excepcion plotly")
        G = nx.Graph()
        Xn = []
        Yn = []

        trace_nodes = dict(type='scatter',
                           x=Xn,
                           y=Yn,
                           mode='markers+ text',
                           marker=dict(symbol='circle-dot', size=35, color='#6959CD'),
                           text=ipsRel,
                           hovertext=orderMacs,
                           textposition='top center',
                           name='Linux/Unix',
                           showlegend=False
                           )
        #
        Xe = []
        Ye = []
        trace_edges = dict(type='scatter',
                           mode='lines',
                           x=Xe,
                           y=Ye,
                           line=dict(width=2, color='#000000'),
                           hoverinfo='none',
                           showlegend=False
                           )

        axis = dict(showline=False,  # hide axis line, grid, ticklabels and  title
                    zeroline=False,
                    showgrid=False,
                    showticklabels=False,
                    title=''
                    )
        layout = dict(title='Network Graph',
                      font=dict(family='Balto'),
                      width=1024,
                      height=700,
                      autosize=False,
                      showlegend=True,
                      xaxis=axis,
                      yaxis=axis,
                      margin=dict(
                          l=40,
                          r=40,
                          b=85,
                          t=100,
                          pad=0,

                      ),
                      hovermode='closest',
                      plot_bgcolor='#E7E7E7',  # set background color
                      )

        fig = dict(data=[trace_edges,trace_nodes] , layout=layout)
        map = plot(figure_or_data=fig, auto_open=False, show_link=False, output_type='div')
        return map

# MapPlotly(nodeList,inputEdges)


