from django.urls import path

from . import views

urlpatterns = [
    path('', views.login, name='login'),
    path('login', views.login, name=''),
    path('index', views.index, name='index'),
    path('logout', views.logout, name='logout'),
    path('rasp/<str:ip>/', views.rasp, name='rasp'),
    path('capture/<str:date>/', views.capture, name='capture'),
    path('ref/', views.refresh, name='refresh'),
    path('info/<str:mac>', views.info, name='info'),
]