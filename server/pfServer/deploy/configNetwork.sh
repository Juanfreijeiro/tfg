#!/bin/bash
#configurando interfaz de red del servidor web

read -p 'Introduce la tarjeta de red: ' cardvar
read -p 'Introduce IP: ' ipvar
read -p 'Introduce máscara de red: ' maskvar
read -p 'Introduce puerta de enlace: ' gwvar
read -p 'Introduce DNS1: ' dns1var
read -p 'Introduce DNS2: ' dns2var

sudo chmod 777 /etc/network/interfaces

echo "source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug $cardvar

iface $cardvar inet static
      address $ipvar
      netmask $maskvar
      gateway $gwvar" > /etc/network/interfaces

sudo chmod 644 /etc/network/interfaces

sudo chmod 777 /etc/resolv.conf

echo "domain tic.pri
search tic.pri
nameserver $dns1var
nameserver $dns2var" > /etc/resolv.conf

sudo chmod 644 /etc/resolv.conf

read -p 'Se va a reiniciar el host....'
sudo reboot