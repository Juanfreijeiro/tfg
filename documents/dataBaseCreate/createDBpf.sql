CREATE TABLE sistemas(
	 cod_sistemas serial PRIMARY KEY,
	 nombre VARCHAR (50) UNIQUE NOT NULL);

	 

CREATE TABLE nodos(
	 cod_nodo serial PRIMARY KEY,
	 mac VARCHAR (50) UNIQUE NOT NULL,
	 cod_sistemas INTEGER NOT NULL,
	 CONSTRAINT sistemas_fk FOREIGN KEY (cod_sistemas)
	      REFERENCES sistemas (cod_sistemas) MATCH SIMPLE
	      ON UPDATE NO ACTION ON DELETE NO ACTION);



CREATE TABLE analisis(
	 cod_analisis serial PRIMARY KEY,
	 fecha_ini TIMESTAMP NOT NULL,
	 fecha_fin TIMESTAMP,
	 cod_usuario INTEGER NOT NULL,
	 CONSTRAINT usuario_fk FOREIGN KEY (cod_usuario)
	  REFERENCES usuarios (cod_usuario) MATCH SIMPLE
	  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE paquetes(
		cod_paquete serial PRIMARY KEY,
		df boolean NOT NULL,
		ttl INTEGER,
		win INTEGER,
		tcpecn VARCHAR(10),
		ip_origen VARCHAR(20),
		ip_destino VARCHAR(20),
		cod_analisis INTEGER NOT NULL,
		cod_nodo INTEGER NOT NULL,
		CONSTRAINT analisis_fk FOREIGN KEY (cod_analisis)
			REFERENCES analisis (cod_analisis) MATCH SIMPLE
			ON UPDATE NO ACTION ON DELETE NO ACTION,
		CONSTRAINT nodo_fk FOREIGN KEY (cod_nodo)
			REFERENCES nodos (cod_nodo) MATCH SIMPLE
			ON UPDATE NO ACTION ON DELETE NO ACTION
		);

CREATE TABLE opcionesTcp(
		cod_opt serial PRIMARY KEY,
		mss INTEGER,
		tsval TIMESTAMP,
		wscale INTEGER);

CREATE TABLE paquete_opciones(
	cod_paquete INTEGER,
	cod_opciones INTEGER,
	ordenTcpOpt VARCHAR(20),
	PRIMARY KEY (cod_paquete, cod_opciones),
	CONSTRAINT paquete_opciones_fk FOREIGN KEY (cod_paquete)
	  REFERENCES paquetes (cod_paquete) MATCH SIMPLE
	  ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT opciones_paquete_fk FOREIGN KEY (cod_opciones)
	  REFERENCES opcionesTcp (cod_opt) MATCH SIMPLE
	  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE usuarios(
	cod_usuario serial PRIMARY KEY,
	nombre VARCHAR(30) NOT NULL,
	contrasena varchar(70) NOT NULL);
	
