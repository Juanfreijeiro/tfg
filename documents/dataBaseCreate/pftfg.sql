CREATE TABLE sistemas(
 cod_sistemas serial PRIMARY KEY,
 ttl VARCHAR (3) UNIQUE NOT NULL,
 nombre VARCHAR (50) UNIQUE NOT NULL,
 opciones VARCHAR (500) UNIQUE NOT NULL
 );

CREATE TABLE nodos(
 cod_nodo serial PRIMARY KEY,
 ip_origen VARCHAR (50) UNIQUE NOT NULL,
 ip_destino VARCHAR (50) NOT NULL,
 cod_sistemas INTEGER NOT NULL,
 CONSTRAINT nodo_sistema_fk FOREIGN KEY (cod_sistemas)
      REFERENCES sistemas (cod_sistemas) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE analisis(
 cod_analisis serial PRIMARY KEY,
 fecha_ini TIMESTAMP NOT NULL,
 fecha_fin TIMESTAMP,
 opciones VARCHAR (500) UNIQUE NOT NULL
 );


CREATE TABLE nodos_analisis(
cod_nodo integer NOT NULL,
cod_analisis integer NOT NULL,
PRIMARY KEY (cod_nodo, cod_analisis),
CONSTRAINT nodo_analisis_fk FOREIGN KEY (cod_nodo)
  REFERENCES nodos (cod_nodo) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
CONSTRAINT analisis_nodo_fk FOREIGN KEY (cod_analisis)
  REFERENCES analisis (cod_analisis) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)