import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import psycopg2
#multythread
from threading import Thread, Condition
#bufferin con ficheros
from io import StringIO

#lock = threading.Lock()

#Bloqueos
#LOCK = threading.Lock()
packets = rdpcap('captures/cabeceras.pcapng')
IP_SRC = "192.168.0.21"

def db_connect():
    try:
        conn = psycopg2.connect("dbname='pftfg' user='postgres' host='localhost' password='abc123.'")
        cur = conn.cursor()
    except:
        print("Fallo al conectar con la base de datos")
    return cur

def crawler_FP(file):
    listIps = []
    uaDetect = {}
    for packet in file:
        if packet.haslayer(IP):
            try:
                if packet.getlayer(IP).src not in listIps:
                    raw = packet.getlayer(Raw)
                    value = raw.fields.values()[0].split()[-1]
                    if value == "Windows":
                        uaDetect[packet.getlayer(IP).src]={"Cliente": packet.getlayer(IP).src,
                                                           "Servidor": packet.getlayer(IP).dst, "SO": value}
                        listIps.append(packet.getlayer(IP).src)
                    elif value == "Linux":
                        uaDetect[packet.getlayer(IP).src] = {"Cliente": packet.getlayer(IP).src,
                                                             "Servidor": packet.getlayer(IP).dst, "SO": value}
                        listIps.append(packet.getlayer(IP).src)
            except:
                continue
    return uaDetect


def ttl_so(file):
    cur = db_connect()
    cur.execute("""SELECT * FROM sistemas""")
    rows = cur.fetchall()
    ttlDict={}
    for i in rows:
        ttlDict[i[1]]=i[2]
    soDetect = {}
    for packet in file:
        if packet.haslayer(IP):
            try:
                if str(packet[IP].ttl) in ttlDict.keys():
                    soDetect[packet[IP].src]=ttlDict[str(packet[IP].ttl)]
            except:
                continue
    return soDetect

def crawlerBrowser(file):
    dictNBT={}
    e=0
    for i in file:
        try:
            if i.haslayer(NBTDatagram) == 1:
                dictNBT[e]={"SourceIP":i[NBTDatagram].SourceIP, "SourceName":i[NBTDatagram].SourceName,
                        "DestinationName":i[NBTDatagram].DestinationName, "DestinationIP": i[IP].dst, "load":i[Raw].load}
                e+=1
        except:
            continue
    return dictNBT

def recNetwork(file):
    privateIpList = [["192.168"],["172.16"],"10"]
    distinctIps = []
    c=0
    for i in file:
        try:
            ip = i[IP].src
            ipBaseList=ip.split(".")
            ipBaseList.pop()
            ipBaseList.pop()
            for a in privateIpList:
                if c == 2:
                    ipBaseList.pop()
                if ipBaseList == a:
                    print("privada")
                else:
                    c+=1
        except:
            continue




def tcp_options(file):
    optionLinuxList = ['MSS','SAckOK','Timestamp','NOP','WScale']
    auxList =[]
    ipList = []
    for i in file:
        try:
            if i.haslayer(TCP):
                if i[IP].src not in ipList:
                    for m in i[TCP].options:
                        b,c = m
                        auxList.append(b)
                    if auxList == optionLinuxList:
                        ipList.append(i[IP].src)
                    auxList = []

        except:
            continue
    print(ipList)







def paint(file):
    ttlDict = ttl_so(file)
    crawlerDict = crawler_FP(file)
    crawlerNBT = crawlerBrowser(file)
    print("------------------------------------Fingerprinting Pasivo-----------------------")
    print("")
    print("----------------------Fingerprinting observando el User-Agent de paquetes HTTP-----------------")
    print("")
    for a in crawlerDict:
        print("Cliente: " + a + " Servidor: " + crawlerDict[a]["Servidor"] + " Sistema: " + crawlerDict[a]["SO"] + "\n")
    print("-----------------------Fingerprinting observando el campo TTL del protocolo IP------------------")
    print("")
    for i in ttlDict:
        print("Cliente :"+ i + " SO " + ttlDict[i])
    print("")
    print("----------------------Informacion de interes (NetBIOS, SMB)----------------------------------------")
    print("")
    for n in crawlerNBT:
        print("-------------------------------------------------------")
        print("Origen : " + crawlerNBT[n]["SourceIP"] + " Nombre cliente : " + crawlerNBT[n]["SourceName"])
        print("Destino : " + crawlerNBT[n]["DestinationIP"] + " Nombre Servidor : " + crawlerNBT[n]["DestinationName"] + "\n")
        print("Informacion :")
        print(crawlerNBT[n]["load"].replace("\x00","") + "\n")

#Get type Ips
def ipType(iprec):
    from IPy import IP
    ip = IP(iprec)
    if ip.iptype() == 'PRIVATE':
        return iprec


#Get Private ips
def getPrivateIps(file):
    ipList=[]
    for packet in file:
        if packet.haslayer(IP):
            try:
                ip = ipType(packet.getlayer(IP).src)
                if (ip != None) and (ip not in ipList) and (ip != "0.0.0.0"):
                    ipList.append(ip)
            except:
                continue
    return ipList


#captura??  capture = sniff(timeout=600)
#conexion ddbb
#diferenciamos tipos IPs
#fingerprinting SO
#almacenamos nodos y su respectivo so en tabla nodos. Si el SO no se
#primero TTL, pasar por crawler y por tcpOptions
#encuentra en la tabla sistemas, Se propone agregarlo.
#almacenamos fechas análisis.

#Lo lanzamos en un nuevo thread?
#Muy lento crear threads cada vez. Vamos a crear un buffer con ficheros de texto.
def startCapture(interval):
    #en min se especifica m. Si no por defecto seran segundos
    values = interval.split(" ")
    if len(values) == 2:
        if values[1] == "m":
            secTime = int(values[0]) * 60
    else:
        secTime = int(values[0])
    #print(secTime)
    capture = sniff(timeout=secTime)
    return capture

class ProducerThread(Thread):
    def run(self):
        global queue
        queue = wdpcap('captures/capturas.pcapng')
        while True:
            n = 1
            print("Productor" + str(1))
            LOCK.acquire()
            queue.write(startCapture("1 m"))
            LOCK.release()
            n+=1

class ConsumerThread(Thread):
    def run(self):
        global queue
        while True:
            if queue:
                lock.acquire()
                num = queue.pop(0)
                print(num)
                lock.release()

def buffering():
    try:
        output = StringIO
        output.write(startCapture("1 m"))
        output.getvalue()
    except ValueError:
        print('Buffer cerrado')
    except:
        print("Algo ha fallado")
        print("Se cierra el buffer")
        output.close()


#ProducerThread().start()
#ConsumerThread().start()

#paint(packets)
#tcp_options(packets)
#recNetwork(packets)
#db_connect()
#print(ttl_so(packets))
#print(str(getPrivateIps(packets)))
#ipType("192.168.1.1")
#print(startCapture("30"))

sniff(prn=crawlerBrowser)




