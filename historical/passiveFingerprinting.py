import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import modelPF, promisc
import os
import gc

id = 0

#Get type Ips
def ipType(iprec):
    from IPy import IP
    ip = IP(iprec)
    if ip.iptype() == 'PRIVATE':
        return iprec



#Get Private ips live
def liveGetPrivateIps(packet):
    if packet.haslayer(IP):
        ip = ipType(packet.getlayer(IP).src)
        if (ip != None) and (ip != "0.0.0.0") and ip != "127.0.0.1":
            return 1
    else:
        return 0



#return value and key for options
def optParser(options):
    auxList={}
    count = 0
    for i in options:
        y,z = i
        auxList[count]={'key': y, 'value': z}
        count = count + 1
    return auxList

#return order by options into manageable unit
def wrapOptions(opt):
    oList = []
    #generate Key list
    for i in opt.values():
        oList.append(i['key'])
    #convert keys into manageable unit
    sort = []
    for o in oList:
        if o == 'NOP':
            sort.append('N')
        elif o == 'Timestamp':
            sort.append('T')
        elif o == 'MSS':
            sort.append('M')
        elif o == 'SAckOK':
            sort.append('S')
        elif o == 'WScale':
            sort.append('W')
        else:
            sort.append('?')
    return sort

#tiempo real DF,TTL,WIN,TCP_ECN,TCP_OPTS,ORD
def recolection(packet):
    recol = {}
    cod_nodo = 0
    #capa física
    if packet.haslayer(Ether) and packet.src != '00:00:00:00:00:00':
        try:
            cod_nodo = modelPF.newNodos(packet.src)
        except:
            pass
        recol[packet.src]={}
        if packet.haslayer(IP) and liveGetPrivateIps(packet):
            recol[packet.src]={'DF':packet[IP].flags, 'TTL': packet[IP].ttl, 'IpOrigen':packet[IP].src,
                               'IpDestino':packet[IP].dst}
            if packet.haslayer(TCP):
                optSize = len(packet[TCP].options)
                if optSize != 0:
                    optSort = wrapOptions(optParser(packet[TCP].options))
                    recol[packet.src]={'DF':packet[IP].flags, 'TTL': packet[IP].ttl, 'IpOrigen':packet[IP].src,
                                   'IpDestino':packet[IP].dst,'Wsize':packet[TCP].window,'Options':packet[TCP].options,
                                       'optSort': optSort}

    #not permit empty dic and dic with empty values and less than 50 ocurrences
    if (len(recol) != 0) and (len(recol[packet.src]) != 0) and (modelPF.getDistinctDic(str(packet.src)) < 10):
            print(recol)
            try:
                opt = recol[packet.src]['optSort']
                opt = True
            except:
                opt = False

            if cod_nodo != 0:
                pk = modelPF.storeNewPack(packet.src, recol, id, cod_nodo)
                op = modelPF.insertOpcionesTcp(packet.src,recol)
                if opt:
                    modelPF.insertPaqueteOpciones(packet.src, recol, pk, op)
            elif len(recol) != 0:
                # preguntamos a la base de datos si el nodo existe
                cod_nodo = modelPF.nodoExist(packet.src)
                pk = modelPF.storeNewPack(packet.src, recol, id, cod_nodo)
                op = modelPF.insertOpcionesTcp(packet.src, recol)
                if opt:
                    modelPF.insertPaqueteOpciones(packet.src, recol, pk, op)
            else:
                pass

            modelPF.storeDic(str(packet.src))
    gc.collect()

def main():
    global id
    #check network target in promisc mode
    promisc.checkCard()
    #file where save ocurrences.
    doc = open("capture.txt","w")
    doc.flush()
    # write date ini-------------------------------------
    id = modelPF.newAnalisis()
    sniff(prn=recolection)
    #write date fin-----------------------------------
    modelPF.dateFinAnalisis(id)


if __name__== "__main__":
    main()