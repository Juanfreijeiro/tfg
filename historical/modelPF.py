import psycopg2
import time
import datetime

#connect to Database
def db_connect():
    try:
        conn = psycopg2.connect("dbname='pf' user='pi' host='localhost' password='sP6Ish0u.'")
        #cur = conn.cursor()
    except:
        print("Fallo al conectar con la base de datos")
    return conn

#store distincts MACs
def storeDic(Dic):
    doc = open("capture.txt","a")
    doc.write(Dic)
    doc.write("\n")
    doc.close()

def getDistinctDic(entryDic):
    doc = open("capture.txt","r")
    readDoc = doc.readlines()
    lines = len(readDoc)
    lst = []
    val = 0
    if lines != 0:
        for dic in readDoc:
            dic = dic.replace("\n", "")
            if dic == entryDic:
                lst.append(dic)
        val = len(lst)
        return val
    else:
        return val

def newAnalisis():
    cod_analisis = 0
    tsIni = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    conn = db_connect()
    sql =  "INSERT INTO analisis(fecha_ini, cod_usuario) VALUES (%s,%s) RETURNING cod_analisis"
    try:
        cur = conn.cursor()
        cur.execute(sql,(tsIni,1))
        cod_analisis = cur.fetchone()[0]
        conn.commit()
        cur.close()
    except:
        print("insert new analisis failure")
    finally:
        if conn is not None:
            conn.close()
    return cod_analisis

def dateFinAnalisis(cod_analisis):
    update_rows = 0
    tsFin = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    conn = db_connect()
    sql = "UPDATE analisis SET fecha_fin = %s WHERE cod_analisis = %s"
    try:
        cur = conn.cursor()
        cur.execute(sql,(tsFin, cod_analisis))
        update_rows = cur.rowcount
        conn.commit()
        cur.close()
    except:
        print("insert fin analisis failure")
    finally:
        if conn is not None:
            conn.close()

    return update_rows

def insertOpcionesTcp(mac, dic):
    cod_opt = 0
    conn = db_connect()

    sqlOpt = "INSERT INTO opcionestcp(mss, tsval, wscale) " \
             "VALUES (%s,%s,%s) RETURNING cod_opt"

    try:
        if not dic[mac]['Options']:
            mss = None
            ts = None
            ws = None
        else:
            for i in dic[mac]['Options']:
                if i[0] == 'MSS':
                    mss = i[1]
                else:
                    mss = None
                if i[0] == 'Timestamp':
                    ts,b = i[1]
                    tsOpt = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                else:
                    ts = None

                if i[0] == 'WScale':
                    ws = i[1]
                else:
                    ws = None
    except:
        pass

    try:
        cur = conn.cursor()
        cur.execute(sqlOpt, (mss, tsOpt, ws))
        cod_opt = cur.fetchone()[0]
        conn.commit()
        cur.close()
    except:
        #print("insert new opt failed")
        pass
    finally:
        if conn is not None:
            conn.close()
    return cod_opt


def insertPaqueteOpciones(mac, dic, cod_paquete, cod_opciones):
    conn = db_connect()

    sql = "INSERT INTO paquete_opciones(cod_paquete, cod_opciones, ordentcpopt) " \
             "VALUES (%s,%s,%s)"

    try:
        optSort = dic[mac]['optSort']
        if optSort != None:
            print("Imprimiendo opciones --> " + str(optSort))
    except:
        optSort = None


    try:
        cur = conn.cursor()
        cur.execute(sql, (cod_paquete[0], cod_opciones, str(optSort)))
        conn.commit()
        cur.close()
    except:
        print("insert new opt_paquete failed :" + str(optSort))
        pass
    finally:
        if conn is not None:
            conn.close()

def storeNewPack(mac, pkcDic, cod_analisis, cod_nodo):
    cod_paquete = 0
    cod_opt = 0
    conn = db_connect()
    sql = "INSERT INTO paquetes(df, ttl, win, tcpecn, ip_origen, ip_destino, cod_analisis, cod_nodo) " \
          "VALUES (%s,%s,%s,%s,%s,%s,%s,%s) RETURNING cod_paquete"

    if len(pkcDic[mac]['IpOrigen']) == 0:
        ip_origen = None
    else:
        ip_origen = pkcDic[mac]['IpOrigen']

    if len(pkcDic[mac]['IpDestino']) == 0:
        ip_destino = None
    else:
        ip_destino = pkcDic[mac]['IpDestino']

    if not pkcDic[mac]['TTL']:
        ttl = None
    else:
        ttl = pkcDic[mac]['TTL']
    try:
        if not pkcDic[mac]['Wsize']:
            win = None
        else:
            win = pkcDic[mac]['Wsize']
    except:
        win  = None

    try:
        df = dFBool(pkcDic[mac]['DF'])
        cur = conn.cursor()
        cur.execute(sql, (df,ttl, win,"",ip_origen, ip_destino ,cod_analisis,cod_nodo))
        cod_paquete = cur.fetchone()[0]
        conn.commit()
        cur.close()
    except:
        print("insert new packet failure")
    finally:
        if conn is not None:
            conn.close()
    return cod_paquete,cod_opt


def dFBool(df):
    dfText = str(df)
    if dfText == '':
        return False
    else:
        return True

def newNodos(mac):
    cod_nodo = 0

    conn = db_connect()

    sql = "INSERT INTO nodos(mac,cod_sistemas) VALUES (%s,%s) RETURNING cod_nodo"
    try:
        cur = conn.cursor()
        cur.execute(sql, (mac,1))
        cod_nodo = cur.fetchone()[0]
        conn.commit()
        cur.close()
    except:
        #print("insert new nodo failure")
        pass
    finally:
        if conn is not None:
            conn.close()
    return cod_nodo

def nodoExist(mac):
    cod_nodo = 0
    conn = db_connect()

    sql = "SELECT cod_nodo FROM nodos WHERE mac = %s"
    try:
        cur = conn.cursor()
        cur.execute(sql,[mac])
        cod_nodo = cur.fetchone()[0]
        conn.commit()
        cur.close()
    except:
        # print("insert new nodo failure")
        pass
    finally:
        if conn is not None:
            conn.close()
    return cod_nodo

#print(nodoExist("00:08:9b:d0:38:79"))