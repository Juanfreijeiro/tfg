#!/bin/bash

echo 'Detenemos la captura de forma correcta...'
echo 'Buscamos los pid de los procesos python...'
PROCESS=`ps aux | grep -i python3 | awk {'print$2'}`
for i in $PROCESS
do
    echo "Deteniendo procesos scann " $i
    sudo kill -2 $i
done

sleep 2
ps aux | grep -i python



