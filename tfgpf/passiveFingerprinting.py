import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
from modelPF import *
from promisc import *
import os
import gc
import signal
import sys
import json
from threading import Thread, Event
logging.basicConfig(filename='output.log', level=logging.DEBUG, format='%(asctime)s %(message)s')
event = Event()
from time import sleep

#e = threading.Event()

#Get type Ips
def ipType(iprec):
    from IPy import IP
    ip = IP(iprec)
    if ip.iptype() == 'PRIVATE':
        return iprec



#Get Private ips live
def liveGetPrivateIps(packet):
    if packet.haslayer(IP):
        ip = ipType(packet.getlayer(IP).src)
        if (ip != None) and (ip != "0.0.0.0") and ip != "127.0.0.1":
            return 1
    else:
        return 0



#return value and key for options
def optParser(options):
    auxList={}
    count = 0
    for i in options:
        y,z = i
        auxList[count]={'key': y, 'value': z}
        count = count + 1
    return auxList

#return order by options into manageable unit
def wrapOptions(opt):
    oList = []
    #generate Key list
    for i in opt.values():
        oList.append(i['key'])
    #convert keys into manageable unit
    sort = []
    for o in oList:
        if o == 'NOP':
            sort.append('N')
        elif o == 'Timestamp':
            sort.append('T')
        elif o == 'MSS':
            sort.append('M')
        elif o == 'SAckOK':
            sort.append('S')
        elif o == 'WScale':
            sort.append('W')
        else:
            sort.append('?')
    return sort

#bit dont fragment
def booleanDf(df):
    if df == 2:
        return True
    else:
        return False

#get User Agent udp packages

def getUserAgent(raw):
    sRaw = str(raw)
    sServer = sRaw.split("SERVER")[1].split("\\n")[0].replace(": ","").replace("\\r","")
    return sServer


#Sniff Thread
class Sniffer(Thread):
    global card

    def __init__(self, interface):
        int = interface
        super().__init__()

        self.interface = interface
        self.stop_sniffer = Event()

    def run(self):
        sniff(iface=self.interface, prn=self.recolection, stop_filter=self.stopSnifferThread)

    def join(self, timeout=None):
        self.stop_sniffer.set()
        super().join(timeout)

    def stopSnifferThread(self, packet):
        return self.stop_sniffer.isSet()

    #tiempo real DF,TTL,WIN,TCP_ECN,TCP_OPTS,ORD
    def recolection(self, packet):
        global fileName
        recol = {}
        #capa fisica
        if packet.haslayer(Ether) and packet.src != '00:00:00:00:00:00':
            recol[packet.src]={}
            if packet.haslayer(IP) and liveGetPrivateIps(packet):
                recol[packet.src]={'DF':str(booleanDf(packet[IP].flags)), 'TTL': packet[IP].ttl, 'IpOrigen':packet[IP].src,
                                   'IpDestino':packet[IP].dst}
                #check Raw information
                try:
                    raw = getUserAgent(packet[Raw].load)
                except IndexError:
                    raw = None
                #print("--COMENZAMOS--")
                if packet.haslayer(TCP):
                    optSize = len(packet[TCP].options)
                    if optSize != 0:
                        optSort = wrapOptions(optParser(packet[TCP].options))
                        #print("-------PAQUETE TCP -----------")
                        recol[packet.src]={'DF': str(booleanDf(packet[IP].flags)), 'TTL': packet[IP].ttl,
                                           'IpOrigen':packet[IP].src, 'IpDestino':packet[IP].dst,'Wsize':packet[TCP].window,
                                           'Options': str(packet[TCP].options),'optSort': optSort,'sport':packet[TCP].sport,
                                           'dport':packet[TCP].dport}
                elif packet.haslayer(UDP):
                    #print("----------------PAQUETE UDP--------------")
                    recol[packet.src] = {'DF':str(booleanDf(packet[IP].flags)),'TTL': packet[IP].ttl,'IpOrigen': packet[IP].src,
                                         'IpDestino': packet[IP].dst,'sport': packet[UDP].sport, 'dport': packet[UDP].dport,
                                         'Raw':raw}
                else:
                    print('Protocolo no gestionado ->' + str(packet[IP].proto))


        #not permit empty dic and dic with empty values and less than 50 ocurrences
        if (len(recol) != 0) and (len(recol[packet.src]) != 0) and (getDistinctDic(str(packet.src)) < 10):
                #print(recol)
                writeToJSONFile(fileName,recol)
                storeDic(str(packet.src))
        gc.collect()


def getCard():
    try:
        card = str(sys.argv[1])
        checkCard(card)
    except:
        print("Especificar tarjeta de red")
        exit(0)



def main():
    global e, fileName, card
    fileNameList=[]
    print("ejecutando PF")
    card=getCard()
    #print(str(sys.argv[1]))
    logging.info("\n")
    logging.info('main --> Comienza ejecución programa \n Comprobamos eth0')
    #check network target in promisc mode
    #file where save ocurrences.h
    doc = open("capture.txt","w")
    doc.flush()
    #class initializer
    Sniffer.interface=card
    sniffer = Sniffer(card)
    #file = FileSplit()
    # write date ini-------------------------------------
    try:
        fileName=createNameJsonFiles()
        logging.info("Creado nuevo fichero de captura")
        writeCharacterList(fileName,"[","w")
        logging.info("main --> Iniciando thread")
        #escribimos fecha inicio
        ids = maxIdMetadata()
        setMetadata(str(ids),"fecha_ini",fileName)
        sniffer.start()
        logging.info("main --> Thread principal creado")
        logging.info("main --> Comenzamos a dividir la captura en ficheros json")
        while True:
            #escribimos nombre fichero en lista
            fileNameList.append(fileName)
            setMetadata(str(ids),"ficheros_captura",fileNameList)
            sleep(getTimeParam())
            fileName = splitJsonFilesInterval(fileName)


    except KeyboardInterrupt:
        deleteEndComma(fileName)
        writeCharacterList(fileName, "]", "a")
        #escribimos fecha fin
        setMetadata(str(ids), "fecha_fin", fileName)
        logging.info("Captura finalizada")
        print("Captura finalizada")
        sniffer.join()


    except Exception as e:
        logging.error("main --> scapy.sniff error %s" % e)
        print("Algo ha fallado")
        #write date fin-----------------------------------
        #dateFinAnalisis(id)
        #id


if __name__== "__main__":
    main()