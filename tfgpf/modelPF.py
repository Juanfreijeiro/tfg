import json
import time
from datetime import date, datetime
import os
import logging
from os.path import  expanduser
logging.basicConfig(filename='output.log', level=logging.DEBUG, format='%(asctime)s %(message)s')
import config as cf
from os.path import  expanduser


#store distincts MACs
def storeDic(Dic):
    doc = open("capture.txt","a")
    doc.write(Dic)
    doc.write("\n")
    doc.close()

def getDistinctDic(entryDic):
    doc = open("capture.txt","r")
    readDoc = doc.readlines()
    lines = len(readDoc)
    lst = []
    val = 0
    if lines != 0:
        for dic in readDoc:
            dic = dic.replace("\n", "")
            if dic == entryDic:
                lst.append(dic)
        val = len(lst)
        return val
    else:
        return val


def dFBool(df):
    dfText = str(df)
    if dfText == '':
        return False
    else:
        return True

#Escribir corchetes inicial y final para crear una lista de diccionarios json
def writeCharacterList(fileName,ch, app):
    f = open("./jsonFiles/" + fileName + ".json", app)
    f.write(ch)
    f.close()

#Eliminar coma final de fichero json
def deleteEndComma(fileName):
    filePathNameWExt = './jsonFiles' + '/' + fileName + '.json'
    with open(filePathNameWExt, "rb+") as filehandle:
        filehandle.seek(-1,os.SEEK_END)
        filehandle.truncate()


#create files json with name date
def createNameJsonFiles():
    #global  father
    #father = []
    now = datetime.now()
    nameDate = datetime.date(now)
    hour = now.hour
    min = now.minute
    formatNameDate = str(nameDate.isoformat())+'-'+str(hour)+':'+str(min)
    return formatNameDate

#check if exist file
def existFileName(name):
    finalName = name
    listFiles = []
    dir = os.listdir("./jsonFiles/")
    #print(dir)
    for file in dir:
        file = file.replace(".json","")
        existCapture = file.split('_')
        if (existCapture[0] == name):
            try:
               listFiles.append(existCapture[1])
            except:
                continue
    if len(listFiles) != 0:
        lastFile = int(max(listFiles)) + 1
        finalName = name + "_" + str(lastFile)
    return finalName

#dividir captura en distintos ficheros en funcion de intervalo de tiempo
def splitJsonFilesInterval(oldFilename):
    deleteEndComma(oldFilename)
    writeCharacterList(oldFilename, "]", "a")
    fileName = createNameJsonFiles()
    logging.info("Dividiendo captura...")
    writeCharacterList(fileName, "[", "w")
    return fileName

def getTimeParam():
    time = cf.client['interval']
    return  time





#write json files
def writeToJSONFile(fileName, data):
    filePathNameWExt = './jsonFiles' + '/' + fileName + '.json'
    with open(filePathNameWExt, 'a') as fp:
        json.dump(data, fp)
        fp.write(",")


#server read json file
def readToJsonFile(fileName):
    with open("./jsonFiles/" + fileName + ".json") as jsonFile:
        json_data = json.load(jsonFile)
        return json_data

#get list to json files
def getJsonFileNames():
    listNames = os.listdir("./jsonFiles")
    return listNames

#read metadataFile
def readMetadata():
    try:
        with open("metadata.json") as meta:
            json_data = json.load(meta)
            return json_data
    except:
        pass

#write metadataFile
def setMetadata(id, pos, data):
    meta=readMetadata()
    if meta == None:
        meta = {}
        meta['0']={'ficheros_captura': [''], 'fecha_fin': '', 'fecha_ini': ''}
    try:
        dic = meta[id]
        dic[pos] = data
    except:
        meta[id]={'ficheros_captura': [''], 'fecha_fin': '', 'fecha_ini': ''}
        meta[id][pos]=data
    with open("metadata.json", 'w') as cap:
        json.dump(meta, cap)

#max id metadataFile
def maxIdMetadata():
    maxId=[]
    meta=readMetadata()
    try:
        for idR in meta.keys():
            maxId.append(int(idR))
        maxMeta = max(maxId)
        if meta[str(maxMeta)]["fecha_ini"] == "":
            return maxMeta
        else:
            return maxMeta+1
    except:
        return 0




#print(maxIdMetadata())

#print(setMetadata("100","fecha_ini","27-08-2018"))

#print(getTimeParam())
#dic = {'00:08:9b:d0:39:6e': {'Raw': "b'NOTIFY * HTTP/1.1\\r\\nHOST:239.255.255.250:1900\\r\\nCACHE-CONTROL:max-age=1810\\r\\nLOCATION:http://192.168.0.3:8200/rootDesc.xml\\r\\nSERVER: 3.4.6-generic Microsoft-Windows/6.1 Windows-Media-Player-DMS/12.0.7601.17514 DLNADOC/1.50 UPnP/1.0 QNAPDLNA/1.0\\r\\nNT:upnp:rootdevice\\r\\nUSN:uuid:4d696e69-444c-164e-9d41-00089bd0396e::upnp:rootdevice\\r\\nNTS:ssdp:alive\\r\\n\\r\\n", 'sport': 33642, 'DF': "<Flag 2 (DF)>", 'TTL': 1, 'IpDestino': "239.255.255.250", 'IpOrigen': "192.168.0.3", 'dport': "1900"}}

#print(existFileName("2018-08-06"))

#print(readToJsonFile("2018-08-06"))

