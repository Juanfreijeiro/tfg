#!/bin/bash
#configurando interfaz wifi

read -p 'Introduce el SSID de la red WIFI: ' ssidvar
read -sp 'Introduce la password de la red WIFI: ' passvar

sudo chmod 777 /etc/wpa_supplicant/wpa_supplicant.conf

echo "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=ES

network={
        ssid=\"$ssidvar\"
        psk=\"$passvar\"
}" > /etc/wpa_supplicant/wpa_supplicant.conf

sudo chmod 644 /etc/wpa_supplicant/wpa_supplicant.conf
echo ""
read -p 'Ip de wlan0 en formato (ip/mask): ' ipvar
read -p 'Ip del gateway: ' gwvar
read -p 'Ip de los DNS: ' dnsvar

sudo chmod 777 /etc/dhcpcd.conf

echo "hostname

clientid

persistent

option rapid_commit

option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes

option ntp_servers

option interface_mtu

require dhcp_server_identifier

slaac private

auto wlan0
interface eth0
iface lo inet loopback

allow-hotplug wlan0
interface wlan0

static ip_address=$ipvar
static routers=$gwvar
static domain_name_servers=$dnsvar
wpa-conf /etc/wpa_supplicant/wpa_suplicant.conf

iface default inet dhcp" > /etc/dhcpcd.conf

sudo chmod 664 /etc/dhcpcd.conf

read -n1 -r -p "Presiona cualquier tecla para reiniciar el dispositivo...." key

sudo reboot 