#!/bin/bash
#conigurando endpoints e intervalo de capturas. Lanzamiento de scann

read -p 'Introduce el intervalo de tiempo que se va a utilizar para dividir los ficheros json (unidades ms): ' tjsonvar

read -p 'Tarjeta de red API: ' ethvar

read -p 'Puerto para los EndPoints (5002):' portvar

ipvar=`ip address show | grep -w $ethvar | cut -d: -f2 | awk '{print $2}' | sed -r '/^\s*$/d'`



if [ -z $portvar ]
    then
        portvar=5002
    fi
echo "#!/usr/bin/env python3

client ={'interval': $tjsonvar,
        'apiIp':'${ipvar::-3}',
        'apiPort': '$portvar'}" > ../config.py

read -p 'Levantar endpoints? (Y/N) ' boolevar

if [ $boolevar = "y" ]
    then
        echo "limpiamos log"
        echo "levantamos endpoints"
        cd ..
        nohup python endPoints.py &
        echo "Endpoints levantados en segundo plano"
        echo ""
    fi

#read -p 'Comenzar escaneo? (Y/N) ' scannBoolevar
#if [ $scannBoolevar = "y" ]
#    then
#        nohup sudo python3 /home/pi/tfg/tfgpf/passiveFingerprinting.py &
#        echo "El escaneo se produce en segundo plano"
#        echo ""
#        ps aux | grep -i python
#    fi