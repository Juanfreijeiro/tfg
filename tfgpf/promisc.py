import os
import sys
import logging
import subprocess
logging.basicConfig(filename='output.log', level=logging.DEBUG, format='%(asctime)s %(message)s')

#card=str(sys.argv[1])
def checkCard(card):
    logging.info('promisc --> Comprobando estado de'+card+'...')
    try:
        subprocess.check_output(['ip', 'link', 'show', card])
    except subprocess.CalledProcessError as err:
        print("Interface not found")
        exit(0)
    promisc = os.system("ip link show "+card+" | grep PROMISC")
    print(promisc)
    try:
        if promisc== 256:
            logging.info("promisc --> Estableciendo tarjeta en modo promiscuo")
            os.system("sudo ip link set "+card+" promisc on")
        logging.info("promisc --> La tarjeta " +card + " ya se encuentra en modo promiscuo")
    except:
        print("network target need reboot")

