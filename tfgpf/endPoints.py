from flask import Flask, request, jsonify
from flask_restful import Resource, Api
#from sqlalchemy import create_engine
from json import dumps
from modelPF import *
import config as cf

app = Flask(__name__)
api = Api(app)

#endpoint get json file point
class Capture(Resource):
    def get(self,scann):
        jsonFile = readToJsonFile(scann)
        return {scann: jsonFile}

#endpoint get name from json files
class FileNames(Resource):
    def get(self):
        jsonFiles = getJsonFileNames()
        return jsonFiles

#endpoint get metadata from captures in raspberry
class Metadata(Resource):
    def get(self):
        meta = readMetadata()
        return meta



api.add_resource(Capture, '/capture/<scann>') # Route_1
api.add_resource(FileNames, '/files') # Route_2
api.add_resource(Metadata, '/metadata') # Route_3


if __name__ == '__main__':
    app.run(host=cf.client['apiIp'], port=cf.client['apiPort'])

# print(readToJsonFile("2018-08-06"))