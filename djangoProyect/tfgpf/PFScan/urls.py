from django.urls import path
from django.conf.urls import url


from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('filters/', views.filters, name='filters'),
    path('ref/', views.refresh, name='refresh'),
]