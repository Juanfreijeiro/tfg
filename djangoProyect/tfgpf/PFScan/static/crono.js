
window.onload = function() {
visor=document.getElementById("reloj"); //localizar pantalla del reloj
//asociar eventos a botones: al pulsar el botón se activa su función.
document.cron.capture.onclick = empezar;
document.cron.stop.onclick = parar;
document.getElementById('#analisisjs').click(consAnalisis())
}

//variables de inicio:
var marcha=0; //control del temporizador
var cro=0; //estado inicial del cronómetro.
//cronometro en marcha. Empezar en 0:
function empezar() {
        //document.cron.capture.disabled = true;
         if (marcha==0) { //solo si el cronómetro esta parado
            emp=new Date() //fecha actual
            elcrono=setInterval(tiempo,10); //función del temporizador.
            marcha=1 //indicamos que se ha puesto en marcha.
             document.getElementById("save").hidden = true;
            }
         }
function tiempo() { //función del temporizador
         actual=new Date() //fecha en el instante
         cro=actual-emp //tiempo transcurrido en milisegundos
         cr=new Date() //fecha donde guardamos el tiempo transcurrido
         cr.setTime(cro)
         cs=cr.getMilliseconds() //milisegundos del cronómetro
         cs=cs/10; //paso a centésimas de segundo.
         cs=Math.round(cs)
         sg=cr.getSeconds(); //segundos del cronómetro
         mn=cr.getMinutes(); //minutos del cronómetro
         ho=cr.getHours()-1; //horas del cronómetro
         if (cs<10) {cs="0"+cs;}  //poner siempre 2 cifras en los números
         if (sg<10) {sg="0"+sg;}
         if (mn<10) {mn="0"+mn;}
         visor.innerHTML=ho+" : "+mn+" : "+sg+" : "+cs; //pasar a pantalla.
         }
//parar el cronómetro
function parar() {

         if (marcha==1) { //sólo si está en funcionamiento
            clearInterval(elcrono); //parar el crono
            marcha=0; //indicar que está parado.
            document.getElementById("save").hidden = false;

            }
         }


function disableButton(){

}

