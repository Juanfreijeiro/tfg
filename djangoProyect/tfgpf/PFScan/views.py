# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.shortcuts import render
from django.http import HttpResponse
from myApp.passiveFingerprinting import *
import logging
logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-10s) %(message)s',)
from django.template import loader
from django.template import RequestContext, Template
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
import threading
import time, datetime

flag = None

def index(request):
    global flag
    nodeList = []
    strDiference = "0: 00 : 00 : 00"
    c = 0
    time =""
    diference=""
    dicList={}
    mensaje = ''
    id = getIdAnalisis()
    #id = 5
    request.session['nodeSesion'] = []
    #template = loader.get_template('index.html')
    t = threading.Thread(target=main, name='PFscan')
    t.setDaemon(True)
    if flag == 1:
        mensaje = "Ya hay una captura en curso. Se debe finalizar la anterior para comenzar una nueva."


    if (request.POST.get('capture') and not flag):
        flag = 1
        try:
            flag = 1
            print("Capturando!!")
            t.start()
            t.join()
        except:
            return HttpResponse("algo ha fallado")
    elif (request.POST.get('stop') and flag):
        flag = 0
        stopThread()
        nodeList = returnNodesbyScann(id)
        for i in nodeList:
            c += 1
            dicList[c]={'mac':i[0],'count':i[1],'ipS':i[2]}
        print("time :" + str(id))
        time = captureTime(id)
        d = diferenceTime(time)
        print("este es el time :")
        print(time)
        if d == None:
            d = "-"
            print("entra por el None")
        else:
            print("tiempo de captura : " + str(d))
            strDiference = str(d)
            print("diferencia :" + strDiference)

    elif request.POST.get('ini'):
        return redirect("/PFScan")

    elif request.POST.get('filter'):
        return redirect("/PFScan/filters")


    #request.session['nodeSesion'] = nodeList

    #return render_to_response('index.html',{}, RequestContext(request))
    return render(request, 'index.html',{'nodos':dicList,'time': strDiference,'mensaje':mensaje})

def filters(request):
    anDic = {}
    anList = []
    a = 0

    anList = getAnalisis()
    for i in anList:

        anDic[a] = {'cod': i[1], 'fecha': i[0]}
        a += 1

    if request.POST.get('ini'):
        return redirect("/PFScan/")

    elif request.POST.get('filter'):
        return redirect("/PFScan/filters/")

    elif request.POST.get("id",False):
        print("furruuuuuuula!!")


    return render(request, 'filter.html',{'analisis':anDic})

def refresh(request):
    nodeList=[]
    dicList={}
    c=0
    id=getIdAnalisis()
    nodeList = returnNodesbyScann(id)
    for i in nodeList:
        c += 1
        dicList[c] = {'mac': i[0], 'count': i[1], 'ipS': i[2]}
        print(dicList)

    return render(request, 'index.html', {'nodos': dicList})



# Create your views here.
